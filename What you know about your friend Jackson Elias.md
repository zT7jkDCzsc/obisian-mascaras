# WHAT YOU KNOW ABOUT YOUR FRIEND JACKSON ELIAS

You remember [[Elias Jackson]] as an African-American man of medium height and build. He has a feisty, friendly air about him. As an orphan in Stratford, Connecticut, he learned to make his own way early in life. He has no living relatives, and no permanent address.

You like him, and value his friendship, even though months and sometimes years separate one meeting from the next. You'd be upset and probably crave vengeance if anything happened to your friend. The world is a better place for having Jackson Elias in it.

Elias speaks several languages fluently and is constantly traveling. He is social, enjoys an occasional drink, and smokes a pipe. A tough, stable, and punctual man, Elias is unafraid of brawls or officials. He is mostly self-educated. Once a skeptic, Elias began to question his stance after the events he witnessed in the mountains of Peru, although in most cases he has failed to find proof of supernatural powers, magic, or dark gods. His well-researched works always seem to reflect first-hand experience. Possibly his greatest flaw is that he's secretive, and never discusses a project until he has a final draft in hand. His writings characterize and analyze death cults. His best-known book is Sons of Death, exposing modern-day Thuggee cults in India. All of his books illustrate how cults manipulate the fears of their followers. Insanity and feelings of inadequacy characterize death cultists; feelings for which they compensate by slaughtering innocents to make themselves feel powerful or chosen. Cults draw the weak- minded, though cult leaders are usually clever and manipulative. When fear of a cult stops, the cult vanishes.

Elias' published works include:
• Skulls Along the River (1910) - exposes headhunter cults in the Amazon basin.
• Masters of the Black Arts (1912) - surveys supposed sorcerous cults throughout history.
The Way of Terror (1913) - analyzes systematization of fear through cult organization; warmly reviewed by George Sorel.
The Smoking Heart (1915) - first half discusses historical Mayan death cults, the second half concerns present-day Central American death cults.
• Sons of Death (1918) - modern-day Thuggees; Elias infiltrated the cult and wrote a book about it.
• Witch Cults of England (1920) - summarizes covens in nine English counties; interviews practicing English witches; Rebecca West thought some of the material trivial and overworked.
The Black Power (1921) - expands upon The Way of Terror and includes some material on Asian and African death cults; includes interviews with several anonymous cult leaders.
• The Hungry Dead (1923) - exposes the modern-day survival of a Peruvian and Bolivian death cult from the time of the conquistadors. It omits many of the weirder details that those who accompanied Elias on his researches may remember, presenting the cult of the kharisiri as a purely human evil.

All of these books are published by Prospero House of New York City, and were edited by owner/editor [[Jonah Kensington]]. Kensington is a good friend of Jackson Elias and someone you may have previously met.