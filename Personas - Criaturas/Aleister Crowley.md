---
alias: ["Crowley"]
---

Oculitista famoso en [[02 London|Londres]], cerca de la [[Fundación Penhew]].
[[Arthur Bidden]] cree que puede ser una fuente de información para la investigación.

Tenemos bastante información sobre él gracias al [[Arthur Bidden|Dr. Bidden]].
- Nacido 12 de octubre de 1875
- En general, lo que pone en la wikipedia: https://es.wikipedia.org/wiki/Aleister_Crowley

