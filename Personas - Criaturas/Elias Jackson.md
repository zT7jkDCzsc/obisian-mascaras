---
alias: ["Elias", "Jackson"]
---

# Apariencia
Según la edición:

![[Elias Jackson V1.png]]

![[Elias Jackson V2.png]]

# Información obtenida:

- Carta mandada a [[Jonah Kensington]]
	- [[1924-08-08 Letter to Jonah Kensington from Elias - Transcript]]

- En algún momento, viajó a [[Nairobi]]
- [[Maeve Abston]]: En 1923 investigaba para el último libro publicado sobre sectas. Iba a ir a [[Kenia]]. Quizás fue por otra cosa.
	- Había ido varias veces a [[Kenia]] a lo largo de su vida.
	- En [[Kenia]] investigó sobre la [[Lengua Sangrienta]] y [[Viento Negro]]
- Última nota suya: "The books are in Carlyle's safe"
	- [[Roger Vane Worthington Carlyle|Carlyle]]

## Desplazamientos
- 1921: Vuelve a ¿?¿?
- 1923-06: Se fue a [[02 London|Londres]]. Pretendía pasar por algunas ciudades de Europa (París, Roma, no se sabe seguro cuales).
- [[1924-08-08]]: Escribe una [[1924-08-08 Carta a Kensington|carta]] a [[Jonah Kensington|Kensington]] desde [[Nairobi]]
- [[1924-09-19]]: Escribe una [[1924-09-19 Carta de Elias desde Hong-Kong|carta]] a [[Jonah Kensington|Kensington]] desde [[Hong-Kong]]
- Se cree que pasó por [[04 El Cairo|El Cairo]] entre medias.
- [[1924-12-xx]] está en [[02 London|Londres]], y desde ahí iba air a [[01 New York|Nueva York]].
- [[1925-01-03]]: [[1925-01-03 Elias manda un telegrama]]
- [[1925-01-15 Jueves]] es [[1925-01-15 Fallece Ellias|asesinado]].

## Funeral
- Se leyó la carta de [[Elias Jackson's Will - Transcript]]
	- Esta se leyó con algunas modificaciones, debido a la relación de Elias con algunos de los personajes.
