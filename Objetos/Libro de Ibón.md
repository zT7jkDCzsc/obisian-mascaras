[[Arthur Bidden|Bidden]] aprendió un conjuro de niebla de este libro.
En el despacho secreto de [[Edward Gavigan|Gavigan]] encontramos una estatua que se parecía a una de las ilustraciones de este libro.

Uno de los símbolos, [[Símbolo escarificado]], de este libro estaba escarificado en la frente de [[Elias Jackson|Jackson]] cuando lo mataron, es el símbolo de la estatua.