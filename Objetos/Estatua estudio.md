En el sanctosantorum de [[Edward Gavigan|Gavigan]]
[[Maeve Abston|Maeve]] escuchó una reverberación de la estatua.

Detrás de la estatua, ponía [[Hofang - Shanghai]]

En la estatua hay un símbolo, que es el que estaba en la frente de [[Elias Jackson|Jackson]] cuando le mataron.

[[Arthur Bidden|Bidden]] vio esta figura en el libro del cuál aprendió el hechizo de niebla. Pueda que tenga que ver con el [[Libro de Ibón]].

