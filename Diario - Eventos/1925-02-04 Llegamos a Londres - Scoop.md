[[1925-02-04 Miércoles]]

Desembarcamos en [[02 London|Londres]], tras viajar en el [[Laurentides]] durante 9 días desde [[01 New York|Nueva York]].

1 Libra equivale a 20 chellines, 5$=1 Libra.

[[Jonah Kensington]], el editor de [[Elias Jackson|Elias]], dijo que tiene un amigo en Londres: [[Mahoney]]

Tenemos pensado visitar la [[Fundación Penhew]], la cual financió la [[Expedición Carlyle]].
Conseguimos una cita con [[Edward Gavigan|Gavigan]] para el día siguiente por la mañana, el [[1925-02-05 Jueves]], tras hablar con su secretario, [[Sr. Kinnedy]].

---
## Entrevista en el scoop.

Más tarde, visitamos el [[El Scoop|Scoop]], dónde nos entrevistamos con [[Mickey Mahoney]] en su despacho. Nos cuenta que:
- [[Elias Jackson|Elias]] estuvo aquí bastante tiempo.
- Parece afligido por la muerte de [[Elias Jackson|Elias]].
- [[Elias Jackson|Elias]] iba a ir a [[01 New York|Nueva York]] tras [[02 London|Londres]].
- Nos muestra unos recortes del [[El Scoop|Scoop]] que [[Elias Jackson|Elias]] investigó.
- [[Elias Jackson|Elias]] estuvo en [[02 London|Londres]] a finales de noviembre de 1924.

Creo ([[Terry]]) que nos está vacilando y que publicará todo lo que decimos. No me fío.

### Recortes que interesaron a Ellias.
- Un pintor, [[Miles Shipley]]: cuadros repulsivos, monstruos extraños, terror, afirma estar en contacto con *otras dimensiones*.
	- [[Scoop - Cuadros grotescos.png]]
	- [[Scoop - Shocking canvases.jpg]]
- Monstruosos asesinatos. 
	- Asesino recibe un disparo, pero consigue escapar. 2 asesinatos. 
	- Agente de Policía, [[Turmwell]] disparó a la criatura.
	- Luna llena, la bestia aulló
- Continúan los asesinatos:
	- Asesinatos en Londres, 24 víctimas en los últimos 3 años.
	- [[Inspector Barrington]]
	- [[Scoop - Continuan los asesinatos.jpg]]
	- [[Scoop - Slaughter continues.jpg]]

---
## Investigamos el telegrama de Elias
En la oficina de telegramas, averiguamos que [[Elias Jackson|Elias]] había mandado su telegrama desde un transatlántico: [[1925-01-03 Elias manda un telegrama]].


---

## Alojamiento en el Savoy
- Preguntas varias:
	- ¿Cuándo estuvo exactamente [[Elias Jackson|Jackson]] en [[02 London|Londres]]? 
	- De dónde salió el telegrama?
	- Fue directo a [[01 New York|NY]]?
	- 