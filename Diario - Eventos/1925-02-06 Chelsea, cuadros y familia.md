Tras el [[1925-02-05 Visitar la fundación, Crowley, saqueo fundación#Incursión en la fundación Penhew|saqueo a la fundación]], decidimos ir hacia [[Chelsea]], para seguir una de las pistas de [[Elias Jackson|Jackson]]. La de los [[Se ponen de moda los cuadros grotescos]].

Abandonamos el coche robado y alquilamos otro por una semana.
Nos alojamos en un bed & breakfast, y conseguimos algo de información sobre [[Miles Shipley]] y [[Berta Shipley]].
