[[1925-02-05 Jueves]]

## Visita a la Fundación Penhew
Por la mañana vamos a la [[Fundación Penhew]]
[[Edward Gavigan]] nos lleva a su despacho, dónde [[Maeve Abston|Maeve]] observa una trampilla abierta en el suelo con dinero dentro, parece que se ha olvidado cerrarla.
### Reunión con Gavigan
Sabe que venimos de [[01 New York|NY]], la [[Erica Carlyle]] le avisó que vendría gente interesada en [[Egipto]]. Dice que necesitariamos a exploradores si queremos seguir la pista.
Tienen la [[Expedición Clive]] ahora mismo en las llanuras de [[Gizá]].
[[Edward Gavigan|Gavigan]] nos dice que [[Roger Vane Worthington Carlyle|Roger Carlyle]] estaba bajo la influencia de [[M'Weru]], y que en [[04 El Cairo|El Cairo]], ==¿la noche se fue con el diario?==
La [[Hypatia Masters|Señorita Hypatia]] se dedicaba a hacer las fotos.
La [[Fundación Penhew]] no financió la expedición, sino que la asesoró.

>[!Opión Terry]
>O dice la verdad mñas absoluta, o el importa una puta mierda lo que le hemos dicho de que quizás esté vivo [[Penhew]].

Hay rumores según los cuales se ha visto a:
- [[Roger Vane Worthington Carlyle|Roger Carlyle]] en [[04 El Cairo|El Cairo]]
- [[Penhew]] en [[Australia]]
- [[Jack Brady]] en [[Hong-Kong]]

Cuando le nombramos a [[Elias Jackson]], tiene un lapso, como si supiese a qué venimos. Dice que [[Elias Jackson|Elias]] estuvo aquí a finales de 1924, cree que le pasaba algo.
Los ojos le van a 1000, está pensando en algo.
> [!Opinión Terry]
> No debemos creer ni una palabra de lo que nos dice. Está pensando en otras cosas.
> - Jackson le acusó de algo? De qué?

- Se encontró con [[Elias Jackson|Elias]] en dos ocasiones, el 2º encuentro desafortunado. El 1º 3 días antes del 2º, este fue más informativo.
	- Creo que [[Elias Jackson|Elias]] averiguó o confirmó algo durante esos tres días, y volvió a enfrentarse con  [[Edward Gavigan|Gavigan]].

[[Edward Gavigan|Gavigan]] quiere saber si tenemos pruebas o fechas para viajar, y que tenemos pensado hacer. Nervioso? Querrá quitarnos de en medio?
De repente está serio y quiere saber dónde nos alojamos, [[Arthur Bidden]] le miente diciendo que estamos en el Hilton, habitación 311.
Dice que lo de [[Elias Jackson|Elias]] le había afectado mucho, pero le supo mal su muerte.


---

## Visita a Aleister Crowley
Por la tarde visitamos a [[Aleister Crowley]]. Creemos que compró uno de los cuadros de [[Miles Shipley]].
[[Aleister Crowley]] no está en [[02 London|Londres]], pero hablamos con su secretaria.
[[Aleister Crowley]] está en el [[04 El Cairo|Cairo]], y quizás esté de vuelta la semana que viene.

Nos permite ver el cuadro de [[Miles Shipley]] en el despacho de [[Aleister Crowley|Crowley]], a cambio de una donación. Representa una "criatura que aparece cómo con unos humanos", según ella.
Cree que el autor sólo vende directamente a la gente, y que viene de las afueras.

### El cuadro de Shipley
El cuadro no es muy grande, en vertical. Cuesta ver la figura, parecen humanos escapando de una criatura oscura, alargada, con capucha, sin pues, medio serpentina, destrozando unos humanos, destripando el humano con una daga. La cara del humano se ve clara, pese a estar trazado con cuatro líneas, pero parece mirar al espectador.
Parece estar firmado.
La gente son sombras, desnudas quizás.
Negro y blanco, con colores tenues.
La figura serpentina es más oscura


[[Rose Mayweather]] cree que la madre del autor está en [[Chelsea]].

---

## Incursión en la fundación Penhew

Sobre las 6 y pico de la tarde vamos a la [[Fundación Penhew]].
Esperamos a que se apaguen las luces y se vaya el servicio de limpieza para entrar por una ventana pasada la medianoche.
Subimos a los despachos.
En el del secretario, el [[Sr. Kinnedy]], encontramos un libro de contabilidad oficial.
En el despacho de [[Edward Gavigan|Gavigan]]:
- [[Maeve Abston|Maeve]] encuentra la fotografía de una casa, la fotografía encarada hacia el asiento de [[Edward Gavigan|Gavigan]]
- Telegrama firmado por el [[Dr. H Clive|Dr. Clive]].
	- [[1924-11-06 Telegrama de Clive]]
- [[Arthur Bidden|Bidden]], en la biblioteca: parte de la biblioteca es una puerta secreta, una escalera sube recta
- [[Maeve Abston|Maeve]]: caja fuerte: unas 100 libras firmadas a mano.
- La escalera lleva a través de un pasadizo a un almacén en la planta superior. En este hay un sarcófago de piedra que [[Arthur Bidden|Bidden]] logra abrir y lleva a lo que parece ser el sanctosanctorum de [[Edward Gavigan|Gavigan]].
	- En su interior encontramos y saqueamos parte de:
		- Pergaminos y libros de ocultismo
		- Un baúl pequeño ornamentado
		- Cajas con velas, tizas, viales rojos.
		- Algunos pergaminos parecen estar hechos con materiales atípicos.
		- Comida y agua
		- En el escritorio un revolver del 32 con cartuchos, pasaportes falsos, 2000 libras en billetes de 5 y 10.
		- Caja de 30x30x15 con papeles (facturas)
		- Cuadros representando torturas y monstruos, pintados por [[Edward Gavigan|Gavigan]].
		- Estatuilla de madera de unos 36cm de alto, murciélago humanoide
		- Tarjeta de visita de [[Empire Spices]]
		- Estatua grande de piedra en una esquina, tapada con una tela. Figura antigua, 200kg más o menos.
			- Mujer entre embarazada y obesa. 
			- De su vientre hinchado se abre una grieta, de la que sale algo.+
		- Varios cuadros, algunos muy antiguos, al menos los que no están pintados por [[Edward Gavigan|Gavigan]].
			- Criatura alada oscura, piel leprosa y quemada
			- Bestia alada con una cola de dragón y mandíbula de colmillos.
			- Grupo de humanos arrodillados, cara se deforma mientras rezan, ojos y boca extremadamente grandes.
			- Uno muy colorido, explosiones de luz. Del centro sale una criatura alta y oscura.
		- Libros:
			- [[Equinox divisée]]
			- [[Books of Dzyan]]
			- [[Libre of Ibonis]] (parte, copia en francés del original en latín).
		- Pergaminos, algunos de conjuros (5, en griego, egipcio y árabe), otros históricos (francés medieval, griego)
		- [[Maeve Abston|Maeve]] escucha una reverberación de la estatua.
			- Detrás de la estatua: [[Ho-Fang - Shanghai]]
			- [[Arthur Bidden|Bidden]] vio esta figura en el libro del cual aprendió el hechizo de niebla. Puede que tenga que ver con el [[Libro de Ibón]]
			- El símbolo de la estatua, [[Símbolo escarificado]], estaba escarificado en el cadaver de [[Elias Jackson|Jackson]]
		- En un baúl ornamentado, dos dagas con intrincada ornamentación en el pomo, son de plata pura y están afiladísimas.

Nos llevamos: dagas, tomos, pergaminos, caja con papeles, fotos delos cuadros, estatuilla de madera con rasgos de murciélago, jarrón con ceniza/polvo, viales, pergaminos.

Cerramos las cosas y salimos, tratamos de llevar las cajas hasta los muelles.
Robamos un coche para cargar las cosas, y nos vamos a [[Chelsea]].