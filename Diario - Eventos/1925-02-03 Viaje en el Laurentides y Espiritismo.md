[[1925-02-03 Martes]]

Es la última noche de viaje en el [[Laurentides]].
Durante los últimos días, hemos conocido a [[Maeve Abston]] y [[Arthur Bidden]]. Estos nos han contado el motivo de su viaje, su relación con [[Elias Jackson|Elias]], y los viajes que hicieron él en el pasado, así como su brutal asesinato.

Esta noche, el Dr. [[Arthur Bidden]] quiere hacer una sesión de espiritismo para hablar con el difunto [[Elias Jackson|Elias]].

Nos cuentan que la última vez que el editor de [[Elias Jackson]] habló con [[Elias Jackson|Elias]], este parecía muy nervioso, o bien había enloquecido algo.

## Espiritismo. Preguntas y respuestas
Pregunta el Dr [[Arthur Bidden]], contesta el espíritu de [[Elias Jackson|Elias]].

- ¿Por qué te mataron?
	- Por qué estaba intentando frustrar algo que planean.
- ¿Qué planean?
	- No lo sé seguro, pero planean algo que pasará dentro de poco. Los seguí por todo el mundo.
- ¿Es un desastre?
	- No, no es un desastre. Algo intentan traer a este mundo.
- ¿Qué era lo siguiente que ibas a hacer?
	- Necesitaba ayuda, ya la había encontrado, necesito a [[Maeve Abston]] y a los demás.
- ¿Que sabía de [[Silas N'Kawane]]?
	- [[Silas N'Kawane]] fue la última persona que localizó/identificó a [[Elias Jackson|Jackson]]. Fue un error.
- ¿A parte de [[Jack Brady]], hay alguno de la [[Expedición Carlyle|expedición]] que esté vivo?
	- Quizás todos.

