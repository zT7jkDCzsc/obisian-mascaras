[[THE SCOOP]]
SHOCKING CANVASES CRAZE
LOCAL ARTIST'S MONSTROUS SCENES MOCK 'SURREALISTS'

NOW COLLECTORS CAN BUY savage scenes which rival or surpass the worst nightmares of the Great War, but which are far more exotic than that grim business.

London artist Mr [[Miles Shipley]]'s work is being sought out by collectors, who have paid up to £300 for individual paintings. This correspondent has seen a number of the works of artist Miles Shipley, and finds them repulsive beyond belief. Maidens ravished, monsters ripping out a man's innards, shadowy grotesque landscapes, and faces grimacing in horror represent only a fraction of Shipley's nightmarish work.

Despite their repellent content, these works are conceived and executed with uncanny verisimilitude. Mr Shipley's imagination is sublime and it is almost as though the artist had worked from photographs of alien places surely never of this Earth! The artist reportedly is in contact with 'other dimensions' in which powerful beings exist, and says he merely renders visible his visions.

Mr Shipley is a working class man without formal artistic training, who has nonetheless made good where thousands have failed. Art critics say that Shipley provides an English answer to the Continental artistic movement of Surrealism, whose controversial practitioners have still to convince John Bull that the way in which a thing is painted is more important than what is painted. A tip of the hat to Mr Shipley for exposing those frauds!