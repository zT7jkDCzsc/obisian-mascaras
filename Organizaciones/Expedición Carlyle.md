## Integrantes
- [[Jack Brady]]
- [[Hypatia Masters]]: fotógrafa
- [[Penhew]] (según lo que dijo [[Edward Gavigan|Gavigan]]).

## Itinerario
- [[01 New York|Nueva York]]: en junio?
- [[02 London|Londres]]
- [[04 El Cairo|El Cairo]]
- [[Kenia]]

## Desaparición/Muerte
El [[Mark Selkirk|Lt. Mark Selkirk]] encontró los cuerpos de la expedición.

## Rumores
Hay rumores según los cuales se ha visto a:
- [[Roger Vane Worthington Carlyle|Roger Carlyle]] en [[04 El Cairo|El Cairo]]
- [[Penhew]] en [[Australia]]
- [[Jack Brady]] en [[Hong-Kong]].
Fuente: [[1925-02-05 Visitar la fundación, Crowley, saqueo fundación#Reunión con Gavigan]]

