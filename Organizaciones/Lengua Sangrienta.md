---
alias: ["La Lengua Sangrienta", "The Bloody Tongue"]
---
Su runa fue escarificada sobre el cadaver de [[Elias Jackson]] cuando fue asesinado.

Esta secta fue expulsada del [[Egipto]] pre-dinástico hacia el sur del continente, encontrándose en la actualidad en el entorno de [[Kenia]].