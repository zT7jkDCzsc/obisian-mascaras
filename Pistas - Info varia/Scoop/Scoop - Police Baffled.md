THE [[El Scoop|Scoop]]
POLICE BAFFLED
BY MONSTROUS MURDERS!
KILLER BEAST SHOT BUT STILL ALIVE?


[[Derwent Valley]] RESIDENTS, shocked last month by two murders and a serious assault on a third victim, are still without a satisfactory explanation or perpetrator of the dreadful attacks.

At that time, Lesser-Edale farmer George Osgood and resident Lydia Perkins were torn to shreds in apparently-unrelated murders on consecutive nights. On the third night, wheelwright Harold Short was nearly killed but managed to drive off his attacker, which he described as a 'grisly creature'.

According to the Lesser-Edale Constabulary, a rabid dog was shot and killed on the night of Mr Short's attack. The police believe the matter to be closed. Nevertheless, local residents have subsequently claimed to have seen and heard a strange beast lurking about the area. Reportedly, the good folk of Lesser-Edale still endure sleepless nights due to the bizarre wailings of the beast on nights of the full moon.

Readers of The Scoop are reminded of their esteemed journal's long-standing Danger Protocols, and are advised that the picturesque cloughs of the Derbyshire Peak District have been declared to be a zone of High Danger! Residents of the Midlands are advised to remain indoors at night and to report all mysterious happenings to the police and to The Scoop.