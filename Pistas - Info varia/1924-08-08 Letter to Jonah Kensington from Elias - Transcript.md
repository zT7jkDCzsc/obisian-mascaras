Transcript from [[1924-08-08 Letter to Jonah Kensington from Elias.jpg]]

August 8, 1924 Nairobi.

Dear [[Jonah Kensington|Jonah]].

Big news! There is a possibility that not all of the members of the [[Expedición Carlyle]] died. 
I have a lead. 
Though the authorities here deny the cult angle, the natives sing a different tune.
You wouldn't believe the stories! Some juicy notes coming your way!
This one may make us all rich!
Blood and kisses, J.