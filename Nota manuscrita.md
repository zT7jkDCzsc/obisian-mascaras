Autor desconocido

Its external angles were magnificent, and most strange; 
by their hideous beauty I was enraptured and enthralled, and I thought myself of the daylight fools who adjudged the housing of this room as mistaken. 

I laughed for the glory they missed. 
Through the twisted door to the jewelled throne of Darkness, I came with all reverence and humility, gaze upon scenes of celestial majesty and rebirth. 
When the six lights were lit and the great words said, then He came, in all the grace and splendour of the Higher Planes, and I longed to sever my veins so that my life might flow into his being, and make part of me a god!