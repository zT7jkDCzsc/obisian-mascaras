
# Content
First Meeting: Jan. 11, 1918 Reference: [[Erica Carlyle]]
Closest Relative: [[Erica Carlyle]]

At his sister's insistence, [[Roger Vane Worthington Carlyle|Mr. Roger Vane Worthington Carlyle]] visited me this morning. He deprecates the importance of his state of mind, but concedes that he has had some trouble sleeping due to a recurring dream in which he hears a distant voice calling his name. (Interestingly the voice uses Mr. Carlyle's second given name, Vane, by which [[Roger Vane Worthington Carlyle|Mr. Carlyle]] admits he always thinks of himself.) [[Roger Vane Worthington Carlyle|Carlyle]]]] moves towards the voice, and has to struggle through a web-like mist in which the caller is **UNKNWOWN**  understood to stand.

The caller is a man-tall, gaunt, dark. An inverted ankh blazes on his forehead. Following the Egyptian theme (C. has had no conscious interest in things Egyptian, he says), the man extends his hands to C., his palms held upward. Pictured on his left palm C. discovers his own face; on the right palm C. sees an unusual, misshapen pyramid.

The caller then brings his hands together, and C. feels himself float off the ground into space. He halts before an assemblage of monstrous figures; figures of humans with animal limbs, with fangs and talons, or of no particular shape at all. All of them circle a pulsating ball of yellow energy, which C. recognizes as another aspect of the calling man. The ball draws him in; he becomes part of it, and sees through eyes not his own. A great triangle appears in the void, its twisted symmetry of the same fashion as the vision of the pyramid. C. then hears the caller say, "And become with me a god". As millions of odd shapes and forms rush into the triangle, C. wakes.

C. does not consider this dream a nightmare, although it upsets his sleep. He says that he revels in it and that it is a genuine calling, although my strong impression is that he actually is undecided about it. An inability to choose seems to characterize much of his life.

[[1918-09-16]] September 18, 1918: He calls her [[M'Weru]], [[M'Weru|Anastasia]], and [[M'Weru|My Priestess]]. He is obsessive about her, as well he might be-exterior devotion is certainly one way to ease the tensions of megalomaniacal contradictions. She is certainly a rival to my authority.


[[1918-12-03]] December 3, 1918: If I do not go, C. threatens exposure. If I do go, all pretense of analysis surely will be lost. What then will be my role?