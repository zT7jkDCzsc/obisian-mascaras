Colaboro con el templo de Bastet, dentro de que un templ de baste no es lo más estructurado del universo.
Ayudo al templo, a la sacerdotisa, [[Neri]].
Me entero de que hubo un problema con un pergamino para los fieles, y que ante el despiste general se lo han llevado, y se ha recuperado por las malas.
Apareció un grupo con los que hubo problemas al inicio, pero luego pidieron perdón y ayuda.
[[Neri]] dijo que ayuda no, pero que serían neutrales.

Me entero de que algo ha pasado. Salvo la suma sacerdotisa, somos todos humanos.
Me hacen llamar. Hay un grupo de gente que ha tenido contacto con el culto, y el simple hecho de que hayan venido a pedir ayuda, implica que el culto debe tener cuidado.
Parece que el grupo ha hecho algo bueno.
Que tienen un hechizo de contacto con la diosa, pero que no se puede usar así como así.

Voy a buscarlos, los encuentro de pura chiripa en un hospital, inconscientes/en coma.
Sé que es un hospital muy bueno, pero no es seguro. Así que decido llevármelos a otro sitio.
Los saco medio en secreto hablando con algunos del hospital, a un hospital más seguro, aunque es un hospital de pobres.
Es la parte final de su recuperación, ya han sido operados.

