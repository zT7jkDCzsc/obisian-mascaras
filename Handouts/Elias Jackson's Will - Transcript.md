Greetings from beyond the grave!

By now you know that all I've really left you is a whole heap of trouble.
If I were still around to have an opinion on the matter, I would understand if you decided to walk away from it all. 
Hell, if I'm dead right now that's a good indication I should have done the same.
But you know me too well, and I know you too well.
If you were the kind of person who always did the sensible thing, we wouldn't be such good friends.

You have been there when I needed you in the past and I hope you will be again, even if it's too late for me.
to I've been pulling threads all over the world and while most of them are still unravelling I think I'm onto something big.
[[Carlton]] and [[Jonah Kensington|Jonah]] can fill in more of the details for you.
I've left some of my papers and notes with them, which should help you work out which hornet's nest you need to poke next.

I trust you to bring my killers to justice.
Of course, I'm assuming I was murdered - it will be just plain embarrasing if I was run over by a trolley car!
Follow my investigation to its bloody end and seek out the truth.
I'm not asking you to finish my book -  none of you can write worth a damn.

Your friend, always,
[[Elias Jackson|Jackson]].